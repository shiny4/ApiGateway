FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
ENV ASPNETCORE_URLS http://*:8008

WORKDIR /app
COPY ./src/ ./src/
COPY *.sln .


RUN dotnet restore
RUN dotnet publish -c release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime
WORKDIR /app
EXPOSE 8100
EXPOSE 8101

ENV ASPNETCORE_ENVIRONMENT=Development
ENV ASPNETCORE_URLS http://*:8100
ENV TZ=Europe/Moscow

COPY --from=build /app/publish ./
CMD dotnet ApiGateway.dll